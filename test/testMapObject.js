const {mapObjects} = require('./../mapObject');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

let cb = (value)=>{
    if (typeof value === 'string'){
        return value + " modified";
    }
    else if (typeof value === 'number'){
        return value * 100;
    }
    else if (typeof value === 'boolean'){
        return !value
    }
    else {
        return value
    }
}

console.log(mapObjects(testObject, cb));