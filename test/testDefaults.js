const {defaults} = require('./../defaults');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const defaultProps = {superHero: "Batman", superVillain: 'Joker', location: "Delhi"};

console.log(defaults(testObject, defaultProps));