function convertToListPairs(obj){

    if (typeof obj !== 'object' || Array.isArray(obj) || obj == null){
        return "argument not an object";
    }

    let result = [], temp=[];
    for (let key in obj){
        temp.push(key);
        temp.push(obj[key]);
        result.push(temp);

        temp = [];
    }

    return result
}

module.exports = {
    convertToListPairs
}