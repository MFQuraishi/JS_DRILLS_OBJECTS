function invert(obj){
    if(obj !== 'object' || Array.isArray(obj) || obj == null){
        return "enter valid object";
    }

    resultObj = {};
    for (key in obj){
        resultObj[obj[key]] = key;
    }

    return resultObj;

}

module.exports = invert;