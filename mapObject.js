function mapObjects(obj, cb){
    if(typeof obj !== 'object' || Array.isArray(obj) || obj === null){
        return "enter valid object"
    }

    resultObj = {};

    for (let key in obj){
        resultObj[key] = cb(obj[key]);
    }

    return resultObj;
}

module.exports = {
    mapObjects
}