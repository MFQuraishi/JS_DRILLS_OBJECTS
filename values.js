function getValues(obj){
    if(typeof obj !== 'object' || Array.isArray(obj) || obj === null){
        return "enter valid object"
    }
    temp = [];
    for (key in obj){
        temp.push(obj[key]);
    }

    return temp;
}

module.exports = {
    getValues
}