function defaults(obj, defaultProps){
    if (typeof obj !== 'object' || Array.isArray(obj) || obj == null){
        return "argument not an object";
    }
    if (typeof defaultProps !== 'object' || Array.isArray(defaultProps) || defaultProps == null){
        return "argument not an object";
    }
    
    for (let key in defaultProps){

        console.log(key);
        console.log(obj[key]);

        if (typeof obj[key] === 'undefined'){
            console.log("entered");
            obj[key] = defaultProps[key]
        }
    }

    return obj;
}

module.exports = {defaults}